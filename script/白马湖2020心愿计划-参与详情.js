return {
  data: function () {
    return {
      ajaxAwait: false
    }
  },
  created: function () {
    var that = this;
    // 获取用户详情
    var id = that.$route.query.id;
    if (!id) { // 跳转到首页
      that.$router.push({
        path: '92E5b15CE6'
      });
    } else {
      that.votev2ActorShowFunc(id);
    }
  },
  methods: {
    votev2ActorShowFunc: function (id) {
      var that = this;
      if (that.ajaxAwait) {
        return null;
      }
      // 获取数据
      that.$options.$lib.Server.fetch('/votev2/actor/show', {
        method: 'POST',
        body: JSON.stringify({
          id: id
        }),
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(function (resource) {
        return resource.json()
      }).then(function (data) {
        console.log('请求结果：', data);
        // 设置用户头像
        if (data.data.user_master.avatar) {
          that.setComponentFieldValueFunc('truck/imagetr', 'url', data.data.user_master.avatar);
        }
        // 设置用户昵称
        if (data.data.user_master.name) {
          that.setComponentFieldValueFunc('truck/textpa', 'text', data.data.user_master.name);
        }
        // 设置参与时间
        if (data.data.create_date) {
          that.setComponentFieldValueFunc('truck/textpa10e', 'text', data.data.create_date);
        }
        // 设置心愿内容
        if (data.data.content) {
          that.setComponentFieldValueFunc('truck/richtextv6', 'text', data.data.content);
        }
        // 设置音频内容
        if (data.data.audio) {
          that.setComponentFieldValueFunc('truck/audioue', 'src', data.data.audio);
        }
        // 设置视频内容
        if (data.data.video) {
          that.setComponentFieldValueFunc('truck/video13a', 'src', data.data.video);
        }
        /*
        // 显示根组件
        var rootComponent = that.$parent.getComponent('root').$refs['root'];
        console.log(rootComponent.style);
        rootComponent.style.display = 'block';
        */
      });
    },
    // 设置组件字段值
    setComponentFieldValueFunc: function (id, field, value) {
      var that = this;
      var component = that.$parent.getComponent(id).$refs[id];
      if (!field) {
        console.log('组件内容：', component);
        return null;
      }
      component[field] = value;
    }
  }
}
