return {
  editorMethods: {
    votev2ActorAddFunc: {}
  },
  data: function () {
    return {
      ajaxAwait: false
    }
  },
  methods: {
    votev2ActorAddFunc: function () {
      var that = this;
      if (that.ajaxAwait) {
        return null;
      }
      // 获取心愿名
      var $name = that.$parent.getComponent('110000/stoneshiploveInput77').$refs['110000/stoneshiploveInput77'];
      var name = $name.getValue();
      if (!name) {
        that.$alert('美好的心愿名更能获得能量哟~');
        return null;
      }
      // 获取姓名
      var $master_name = that.$parent.getComponent('110000/stoneshiploveInput77n4').$refs['110000/stoneshiploveInput77n4'];
      var master_name = $master_name.getValue();
      if (!master_name) {
        that.$alert('请填写真实姓名仅用于获奖联系');
        return null;
      }
      // 获取电话
      var $master_phone = that.$parent.getComponent('110000/stoneshiploveInput77n4ke').$refs['110000/stoneshiploveInput77n4ke'];
      var master_phone = $master_phone.getValue();
      if (!master_phone) {
        that.$alert('请填写手机号码仅用于获奖联系');
        return null;
      }
      // 获取心愿内容
      var $content = that.$parent.getComponent('110000/stoneshiploveInput77n4keh8').$refs['110000/stoneshiploveInput77n4keh8'];
      var content = $content.getValue();
      if (!content) {
        that.$alert('美好的心愿内容是必不可少的哟~');
        return null;
      }
      // 获取视频
      var video = that.dataHubGet('actor_video')
      // 获取音频
      var audio = that.dataHubGet('actor_audio')
      // 获取图片
      var image = that.dataHubGet('actor_image')

      // 提交数据
      that.$options.$lib.Server.fetch('/votev2/actor/add/main/5', {
        method: 'POST',
        body: JSON.stringify({
          info: {
            name: name,
            image: image,
            video: video,
            audio: audio,
            content: content,
            master_name: master_name,
            master_phone: master_phone
          }
        }),
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(function (resource) {
        return resource.json()
      }).then(function (data) {
        console.log('请求结果：', data);
        if (data.code == 0 && data.data == 'weixin_login') { // 微信登陆
          that.$confirm(data.msg, function () {
            window.location.href = data.url + '?callback=' + encodeURIComponent(window.location.href)
          }, function () {
            console.log('取消')
          });
          return null;
        }
        that.$alert(data.msg, function () {
          if (data.code == 1) { // 成功
            that.$router.push({
              path: 'cB20f2Fcc8',
              query: {
                id: data.data.id
              }
            })
          }
        });
      });
    }
  }
}
