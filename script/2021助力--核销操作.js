return {
    editorMethods: {
        assistPrizeApplyFunc: {
            label: '核销操作',
            params: [
                {
                    label: '接口地址',
                    desc: '请求接口地址',
                    type: 'string',
                    default: ''
                }
            ]
        }
    },
    methods:{
        assistPrizeApplyFunc:function(url, apply_code){
            var that = this;
            // 判断是否登陆
            var token = that.dataHubGet('wx_token');
            if (!token) {
                return false;
            }
            apply_code = apply_code || '';
            if (!apply_code) {
                that.$prompt({
                    title: '奖品核销',
                    msg: '请至一楼客服台核销领取'
                }, function (result) {
                    console.log('操作结果：', result);
                    if (!result.value) {
                        that.$alert('请输入核销密码');
                        return false;
                    }
                    that.assistPrizeApplyFunc(url, result.value);
                });
                return false;
            }

            that.$loading();
            that.$options.$lib.Server.fetch(url, {
                method: 'post',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Token': token
                },
                body: JSON.stringify({  //这里是post请求的请求体
                    id: that.dataHubGet('assist_prize_actor_id'),
                    apply_code: apply_code
                })
            }).then(function(resource){
                that.$hideLoading();
                return resource.json()
            }).then(function (response) {
                that.$alert(response.msg, function () {
                    if (response.code == 1) {
                        window.location.reload();
                    }
                });
            });
        }
    }
}