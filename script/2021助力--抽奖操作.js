return {
    editorMethods: {
        assistPrizeLuckFunc: {
            label: '抽奖操作',
            params: [
                {
                    label: '接口地址',
                    desc: '请求接口地址',
                    type: 'string',
                    default: ''
                }
            ]
        }
    },
    methods:{
        assistPrizeLuckFunc:function(url){
            var that = this;
            // 判断是否登陆
            var token = that.dataHubGet('wx_token');
            if (!token) {
                return false;
            }
            that.$loading();
            that.$options.$lib.Server.fetch(url, {
                method: 'post',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Token': token
                },
                body: JSON.stringify({  //这里是post请求的请求体
                    id: that.dataHubGet('assist_prize_actor_id')
                })
            }).then(function(resource){
                that.$hideLoading();
                return resource.json()
            }).then(function (response) {
                if (response.data == 'mc_login') {
                    that.$confirm({
                        msg: response.msg,
                        title: '系统提示'
                    }, function () {
                        window.history.forward(1);
                        window.location.href = response.url;
                    }, function () {});
                    return false;
                }
                if (response.code == 1) {
                    that.$alert('恭喜您获得【'+ response.data.name +'】', function () {
                        window.location.reload();
                    });
                    return false;
                }
                that.$alert(response.msg, function () {
                    window.location.reload();
                });
            });
        }
    }
}