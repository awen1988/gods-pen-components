let that = null;
return {
    editorMethods: {
        testFunc: {
            label: '测试操作',
            params: [
                {
                    label: '接口地址',
                    desc: '请求接口地址',
                    type: Object,
                    default: ''
                }
            ]
        }
    },
    data() {
        return {
            detail: Math.ceil(Math.random()*10)
        }
    },
    mounted: function () {
        this.$nextTick(function () {
            that = JSON.stringify(this.$data);
            console.log('拷贝后', that)
            console.log('创建时', this)
            console.log('创建时scope', this.scope)
            console.log('创建时inscope', this.inscope)
        });
        // this.detail = this.scope;
    },
    methods:{
        testFunc: function(ccc) {
            console.log('传入参数：', ccc)
            console.log('代理值：', that)
            console.log('当前组件：', this)
            console.log('当前数据：', this.detail)
            console.log('当前scope：', this.scope.id)
        }
    }
}