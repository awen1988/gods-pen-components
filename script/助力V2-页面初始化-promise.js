/**
 * 在vue的
 */
return {
  data: function () {
    return {
      // 接口域名
      api_url: 'https://weixin.jlqfybjy.xyz',
      // 活动ID
      main_id: 1,
      // 用户TOKEN
      user_token: null
    }
  },
  created: function () {
    var that = this
    // 微信登陆
    that.wxLoginFunc().then(function (token) {
      if (token) {
        that.user_token = token
        that.dataHubSet('root.user_token', token)
      }
      // 活动初始化
      // return that.mainInitFunc()
    }).then(function (data) {
      console.log('活动数据：', data)
    })
  },
  methods: {
    /**
     * 判断是否为微信客户端
     * @returns {boolean}
     */
    isWxClientFunc: function () {
      var ua = window.navigator.userAgent.toLowerCase()
      if (ua.match(/MicroMessenger/i) == 'micromessenger') { // eslint-disable-line
        return true
      }
      return false
    },
    /**
     * 微信登陆操作
     */
    wxLoginFunc: function () {
      var that = this
      return new Promise(function (resolve, reject) {
        if (!that.isWxClientFunc()) {
          return reject(false)
        }
        var wx_user_token = that.$route.query.wx_user_token
        console.log('路由wx_user_token', wx_user_token)
        if (wx_user_token) {
          // 存入本地缓存
          localStorage.setItem('wx_user_token_' + that.main_id, wx_user_token)
          that.wxSafeFunc()
          return resolve(wx_user_token)
        }
        wx_user_token = localStorage.getItem('wx_user_token_' + that.main_id)
        console.log('缓存wx_user_token', wx_user_token)
        if (wx_user_token) {
          return resolve(wx_user_token)
        }
        reject(false)
        // 去请求user_token
        window.location.href = 'https://weixin.jlqfybjy.xyz/assistv2/main/wx_auth/id/' + that.main_id + '?callback=' + encodeURIComponent(window.location.href)
        return
      }).catch(function (error) {
        console.log(error)
      })
    },
    /**
     * 微信安全连接处理
     */
    wxSafeFunc: function () {
      // 抹掉URL数据
      var nowLink = window.location.href;
      var newLink = nowLink.replace(/[?|&]wx_user_token=[%+-_0-9a-zA-z|=]*/, '')
      if (nowLink !== newLink) {
        // region 禁止浏览器后退
        history.pushState(null, null, newLink)
        window.addEventListener('popstate', function () {
          history.pushState(null, null, newLink)
        })
        // endregion
        window.location.href = newLink
      }
      return false
    },
    /**
     * 主数据初始化
     */
    mainInitFunc: function () {
      var that = this
      var lib = that.$options.$lib
      return lib.Server.fetch(that.api_url + '/assistv2/main/show/id/' + that.main_id, {
        method: 'get',
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'User-Token': that.user_token
        }
      }).then(function(res){
        return res.json()
      })
    }
  }
}
