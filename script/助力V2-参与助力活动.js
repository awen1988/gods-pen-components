return {
  editorMethods: {              // 此项配置自定义方法的在组件配置面板如何展示
    actorAddFunc: {              // 方法名，对应于 `methods` 内的某方法
      label: '参与助力活动',         // 自定义方法显示名
      params: [                 // 参数列表，对象数组
        {
          label: '请求地址',     // 参数1的名称
          desc: '请求接口地址',  // 参数1的描述
          type: 'string',       // 参数1的类型，支持`string|number|boolean|array|object`
          default: ''           // 参数1默认值
        },
        {
          label: '活动ID',
          desc: '参与的活动ID',
          type: 'number',
          default: 0
        }
      ]
    }
  },
  methods:{
    actorAddFunc:function(url, id){
      var that = this
      console.log('页面中的方法：', that.dynamicLoadCss)
      console.log(that.$dialog)
      that.$dialog.toast({
        mes: '保存成功',
        timeout: 1500,
        icon: 'success'
      });
      var lib = that.$options.$lib
      lib.Server.fetch(url, {
        method: 'post',
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        // credentials: 'include',
        body: JSON.stringify({  //这里是post请求的请求体
          main: id
        })
      }).then(function(res){
        console.log(res)
      })
    }
  }
}
return node
