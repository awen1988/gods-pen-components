return {
  created: function () {
    this.initFunc()
  },
  methods: {
    initFunc: function () {
      var that = this;
      that.$options.$lib.Server.fetch('/votev2/actor/init_add/main/5', {
        method: 'GET',
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(function (resource) {
        return resource.json()
      }).then(function (result) {
        if (result.code != 1) {
          // 跳转到错误页面
          that.$alert('数据加载失败，请联系微信：229417598反馈，谢谢~', function () {
          });
          return null;
        }
        if (!result.data.user_info) { // 跳转到登陆
          if (!result.data.login_link) {
            that.$alert('自动登陆失败，请联系微信：229417598反馈，谢谢~', function () {
            });
          }
          // 跳转到登陆
          window.location.href = result.data.login_link + '?callback=' + encodeURIComponent(window.location.href)
          return null;
        }
        if (result.data.actor_info) { // 已报名
          that.$router.push({
            path: 'cB20f2Fcc8',
            query: {
              id: result.data.actor_info.id
            }
          })
          return null;
        }
        // 设置数据总线
        that.dataHubSet('qiniu_config', result.data.qiniu_config)
      })
    }
  }
}
