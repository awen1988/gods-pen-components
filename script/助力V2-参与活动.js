return {
  editorMethods: {              // 此项配置自定义方法的在组件配置面板如何展示
    actorAddFunc: {              // 方法名，对应于 `methods` 内的某方法
      label: '参与活动',         // 自定义方法显示名
      params: [                 // 参数列表，对象数组
        {
          label: '接口地址',     // 参数1的名称
          desc: '请求接口地址',  // 参数1的描述
          type: 'string',       // 参数1的类型，支持`string|number|boolean|array|object`
          default: ''           // 参数1默认值
        },
        {
          label: '活动ID',
          desc: '参与的活动ID',
          type: 'number',
          default: 0
        }
      ]
    }
  },
  methods:{
    actorAddFunc:function(url, id){
      var that = this
      var lib = that.$options.$lib
      lib.Server.fetch(url, {
        method: 'post',
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'User-Token': that.dataHubGet('root.user_token')
        },
        body: JSON.stringify({  //这里是post请求的请求体
          main: id
        })
      }).then(function(resource){
        return resource.json()
      }).then(function (resource) {
        that.$alert(resource.msg, function () {
          if (resource.data) {
            that.$router.push({
              path: 'Eac7d9648b',
              query: {
                id: resource.data.id
              }
            })
            return
          }
        })
      })
    }
  }
}
return node
