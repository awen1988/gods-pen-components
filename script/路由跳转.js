return {
    editorMethods: {              // 此项配置自定义方法的在组件配置面板如何展示
        projectJump: {              // 方法名，对应于 `methods` 内的某方法
            label: '内部跳转',         // 自定义方法显示名
            params: [                 // 参数列表，对象数组
                {
                    label: '跳转地址',     // 参数1的名称
                    desc: '项目相对地址',  // 参数1的描述
                    type: 'string',       // 参数1的类型，支持`string|number|boolean|array|object`
                    default: ''           // 参数1默认值
                },
                {
                    label: '参数',
                    desc: 'query形式参数',
                    type: 'object',
                    default: {}
                }
            ]
        }
    },
    methods:{
        projectJump:function(path,query){
            this.$router.push({
                path:path,
                query:query
            })
        }
    }
}
return node