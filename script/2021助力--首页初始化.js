/**
 * 助力主页初始化执行
 * 2021-08-09
 */

return {
    created: function () {
        // 页面链接安全处理
        var wxToken = this.wxTokenFunc();
        if (wxToken) {
            // 进行安全处理
            this.wxSafeFunc();
        }
    }
}