return {
    editorMethods: {
        openLinkFunc: {
            label: '链接跳转',
            params: [
                {
                    label: '跳转地址',
                    desc: '链接跳转地址，内部链接输入页面ID',
                    type: 'string',
                    default: ''
                }
            ]
        }
    },
    methods:{
        openLinkFunc: function (link){
            this.$loading();
            window.location.href = '/m-view/' + link;
        }
    }
}