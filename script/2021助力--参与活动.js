return {
    // 参数定义
    props: {
        // 参与详情页
        pageActorShow: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '详情页面',
                desc: '参与详情页面路由ID'
            }
        },
    },
    editorMethods: {
        actorAddFunc: {
            label: '参与活动',
            params: [
                {
                    label: '接口地址',
                    desc: '请求接口地址',
                    type: 'string',
                    default: ''
                },
                {
                    label: '活动ID',
                    desc: '参与的活动ID',
                    type: 'number',
                    default: 0
                }
            ]
        }
    },
    methods:{
        actorAddFunc:function(url, id){
            var that = this;
            // 判断是否登陆
            var root = that.$parent.getComponent('root', true);
            var token = root.wxLoginFunc();
            if (!token) {
                return false;
            }
            that.$loading();
            that.$options.$lib.Server.fetch(url, {
                method: 'post',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Token': token
                },
                body: JSON.stringify({
                    main: id
                })
            }).then(function(resource){
                that.$hideLoading();
                return resource.json()
            }).then(function (response) {
                if (response.code == 1) {
                    window.history.forward(1);
                    window.location.href = '/m-view/' + that.pageActorShow + '?id=' + response.data.id;
                    return true;
                }
                if (response.data == 'mc_login') {
                    that.$confirm({
                        msg: response.msg,
                        title: '系统提示'
                    }, function () {
                        window.history.forward(1);
                        window.location.href = response.url;
                    }, function () {});
                    return false;
                }
                that.$alert(response.msg);
            });
        }
    }
}