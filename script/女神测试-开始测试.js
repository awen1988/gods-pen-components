return {
    editorMethods: {
        startFunc: {
            label: '开始测试'
        }
    },
    methods: {
        startFunc: function () {
            console.log('开始测试')
            // 判断是否有微信TOKEN
            let root = this.$parent.getComponent('root', true)
            let token = root.wxIsloginFunc()
            console.log('微信TOKEN', token)
            if (!token) {
                this.$dialog.confirm({
                    title: '登陆提示',
                    mes: '系统需要您登陆后继续',
                    opts: [
                        {
                            txt: '我就看看',
                            color: false,
                            callback: function () {}
                        },
                        {
                            txt: '授权登陆',
                            color: true,
                            callback: () => {
                                root.wxLoginFunc()
                            }
                        }
                    ]
                })
                return null
            }
            this.$router.push({
                path: 'c39FD96982'
            })
        }
    }
}