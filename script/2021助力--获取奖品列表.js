return {
    props: {
        assistPrizeListApi: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '数据接口',
                desc: '奖品列表接口地址'
            }
        },
        showComponentId: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '展示组件',
                desc: '列表展示组件ID'
            }
        }
    },
    editorMethods: {
        assistPrizeListFunc: {
            label: '奖品列表',
            params: [
                {
                    label: '接口地址',
                    desc: '请求接口地址',
                    type: 'string',
                    default: ''
                }
            ]
        }
    },
    methods:{
        assistPrizeListFunc: function (url) {
            var that = this;
            url = url || that.assistPrizeListApi;
            that.$loading();
            var headers = {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            };
            that.$options.$lib.Server.fetch(url, {
                method: 'GET',
                headers: headers
            }).then(function(resource){
                that.$hideLoading();
                return resource.json()
            }).then(function (response) {
                if (response.code == 1 && that.showComponentId) {
                    that.$parent.getComponent(that.showComponentId).info.props.list = response.data.items;
                }
            });
        }
    }
}