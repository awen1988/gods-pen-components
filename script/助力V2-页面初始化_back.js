return {
    mounted () {
        var that = this;
        var lib = that.$options.$lib
        // 加载ydui
        that.dynamicLoadCss('https://weixin.jlqfybjy.xyz/static/ydui/ydui.px.css')
        lib.Util.loadJs('https://weixin.jlqfybjy.xyz/static/ydui/ydui.px.js').then(function () {

        });
    },
    methods: {
        /**
         * 动态加载CSS
         * @param {string} url 样式地址
         */
        dynamicLoadCss: function (url) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.type='text/css';
            link.rel = 'stylesheet';
            link.href = url;
            head.appendChild(link);
        }
    }
}