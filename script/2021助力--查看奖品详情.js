/**
 * 非异步请求，直接弹窗查看数据的形式
 */
return {
    props: {
        showComponentId: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '展示组件',
                desc: '列表展示组件ID'
            }
        }
    },
    editorMethods: {
        assistPrizeShowFunc: {
            label: '奖品详情'
        }
    },
    methods: {
        assistPrizeShowFunc: function () {
            // 奖数据设置到总线
            this.dataHubSet('prize', this.scope);
            // 显示奖品详情
            this.$parent.getComponent(this.showComponentId).visible = true;
        }
    }
}