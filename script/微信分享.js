return {
    props: {
        wxId: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '微信ID',
                desc: '微信分享公众号ID'
            }
        },
        wxConfigApi: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '配置接口',
                desc: '微信分享配置接口地址'
            }
        },
        wxShareTitle: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '分享标题',
                desc: '微信分享标题'
            }
        },
        wxShareDesc: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '分享描述',
                desc: '微信分享描述'
            }
        },
        wxShareImage: {
            type: String,
            default: '',
            editor: {
                type: 'image',
                label: '分享图片',
                desc: '微信分享图片'
            }
        },
        wxShareLink: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '分享链接',
                desc: '微信分享链接'
            }
        }
    },
    methods: {
        wxShareFunc: function () {
            var that = this;
            that.$options.$lib.Util.loadJs('https://res.wx.qq.com/open/js/jweixin-1.6.0.js').then(function () {
                var nowLink = window.location.href;
                that.$options.$lib.Server.fetch(that.wxConfigApi, {
                    method: 'post',
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest',
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({  //这里是post请求的请求体
                        wx_id: that.wxId,
                        wx_share_sign_link: nowLink
                    })
                }).then(function(resource){
                    return resource.json()
                }).then(function (response) {
                    if (response.code == 1) {
                        wx.config(JSON.parse(response.data));
                        wx.ready(function() {
                            // 分享到朋友圈
                            wx.onMenuShareTimeline({
                                title: that.wxShareTitle,
                                link: that.wxShareLink || nowLink,
                                imgUrl: that.wxShareImage,
                                success: function () {
                                    // 用户确认后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消后执行的回调函数
                                }
                            });
                            //分享给朋友
                            wx.onMenuShareAppMessage({
                                title: that.wxShareTitle,
                                desc: that.wxShareDesc,
                                link: that.wxShareLink || nowLink,
                                imgUrl: that.wxShareImage,
                                type: 'link',
                                dataUrl: '',
                                success: function () {
                                    // 用户确认后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消后执行的回调函数
                                }
                            });
                        });
                    }
                });
            });
        }
    }
}