return {
  editorMethods: {
    playAudioFunc: {}
  },
  methods: {
    playAudioFunc: function () {
      var that = this;
      // 获取音频组件
      var audioComponent = that.$parent.getComponent('truck/audioue').$refs['truck/audioue'];
      audioComponent.tooglePlaying();
    }
  }
}
