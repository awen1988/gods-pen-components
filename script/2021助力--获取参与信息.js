return {
    props: {
        // 接口地址
        assistActorShowApi: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '参与详情',
                desc: '参与详情数据接口地址'
            }
        },
        // 展示映射
        assistActorShowField: {
            type: Object,
            editor: {
                type: 'object',
                label: '字段映射',
                desc: '数据展示映射关系'
            }
        }
    },
    methods: {
        assistActorShowFunc: function (id) {
            id = id || 0;
            if (!id) {
                return false;
            }
            var that = this;
            var token = that.dataHubGet('wx_token');
            if (!token) {
                return false;
            }
            that.$loading();
            var headers = {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Token': token
            };
            that.$options.$lib.Server.fetch(that.assistActorShowApi + '?id=' + id, {
                method: 'GET',
                headers: headers
            }).then(function (resource) {
                that.$hideLoading();
                return resource.json()
            }).then(function (response) {
                // console.log('请求结果：', response);
                // 设置总线数据
                that.dataHubSet('assist_prize_actor_id', response.data.prize_actor.id);
                if (that.assistActorShowField) {
                    for (var key in that.assistActorShowField) {
                        var value = eval('response.data.' + that.assistActorShowField[key].data);
                        that.setComponentFieldValueFunc(key, that.assistActorShowField[key].field, value);
                    }
                }
                // 设置助力进度
                var recordRate = (response.data.prize_actor.record_count * 100 / response.data.prize_actor.record_target) + '%';
                that.$parent.getComponent('truck/emptyContainermn').info.style.height = recordRate;
                // 设置助力状态
                if (response.data.prize_actor.record_count >= 5) {
                    that.showComponentById('truck/image6xgr');
                } else if (response.data.prize_actor.record_count >= 3) {
                    that.showComponentById('truck/image6x');
                } else if (response.data.prize_actor.record_count >= 1) {
                    that.showComponentById('truck/imageo3');
                }
                if (response.data.prize_actor.record_count < response.data.prize_actor.record_target) {
                    // 助力中
                    if (response.data.is_master) {
                        that.showComponentById('truck/imageo4');
                    } else {
                        that.showComponentById('truck/imageo4luwv');
                    }
                } else if (response.data.prize_actor.record_count >= response.data.prize_actor.record_target && !response.data.prize_actor.prize_id) {
                    // 助力完成待抽奖
                    if (response.data.is_master) {
                        that.showComponentById('truck/imageo4lu');
                    } else {
                        that.showComponentById('truck/imageo4gk');
                    }
                } else if (response.data.prize_actor.prize_id) {
                    // 我的奖品
                    if (response.data.is_master) {
                        if (response.data.prize_actor.prize_type == 'goods') {
                            that.showComponentById('truck/textor');
                        } else {
                            that.showComponentById('truck/textormp');
                        }
                        if (response.data.prize_actor.apply_status == 1) {
                            that.showComponentById('truck/buttonhf');
                        } else {
                            that.showComponentById('truck/buttonhfhd');
                        }
                    } else {
                        that.showComponentById('truck/imageo4gk');
                    }
                }
            });
        },
        // 设置组件字段值
        setComponentFieldValueFunc: function (id, field, value) {
            var component = this.$parent.getComponent(id).info.props;
            if (!field) {
                return null;
            }
            component[field] = value.toString();
        }
    }
}