return {
  data: function () {}
  created: function () {
    var that = this
    // 微信登陆
    that.wxLoginFunc().then(function (token) {
      if (token) {
        that.user_token = token
        that.dataHubSet('root.user_token', token)
      }
      // 活动初始化
      // return that.mainInitFunc()
    }).then(function (data) {
      console.log('活动数据：', data)
    })
  },
}
