/**
 * 在vue的
 */
return {
  data: function () {
    return {
      main_id: 1
    }
  },
  created: function () {
    var that = this
    // 判断用户是否登录
    var wx_user_token = localStorage.getItem('wx_user_token_' + that.main_id)
    if (that.isWxClientFunc() && !wx_user_token) {
      that.wxLoginFunc()
    }
    // 处理安全连接
    that.wxSafeFunc()
  },
  methods: {
    /**
     * 判断是否为微信客户端
     * @returns {boolean}
     */
    isWxClientFunc: function () {
      var ua = window.navigator.userAgent.toLowerCase()
      if (ua.match(/MicroMessenger/i) == 'micromessenger') { // eslint-disable-line
        return true
      }
      return false
    },
    /**
     *
     */
    wxLoginFunc: function () {
      console.log('微信登陆');
      var that = this
      var wx_user_token = that.$route.query.wx_user_token
      console.log('微信TOKEN', wx_user_token)
      if (wx_user_token) {
        // 存入本地缓存
        localStorage.setItem('wx_user_token_' + that.main_id, wx_user_token)
        that.wxSafeFunc()
        return true
      }
      // 去请求user_token
      window.location.href = 'https://weixin.jlqfybjy.xyz/assistv2/main/wx_auth/id/' + that.main_id + '?callback=' + encodeURIComponent(window.location.href)
      return false
    },
    wxSafeFunc: function () {
      // 抹掉URL数据
      var nowLink = window.location.href;
      var newLink = nowLink.replace(/[?|&]wx_user_token=[%+-_0-9a-zA-z|=]*/, '')
      if (nowLink !== newLink) {
        // region 禁止浏览器后退
        history.pushState(null, null, newLink)
        window.addEventListener('popstate', function () {
          history.pushState(null, null, newLink)
        })
        // endregion
        window.location.href = newLink
      }
      return false
    }
  }
}
