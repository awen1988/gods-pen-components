return {
    props: {
        // 首页路由
        pageMainShow: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '活动首页',
                desc: '活动首页路由ID'
            }
        },
    },
    created: function () {
        // 获取参与详情
        var id = this.$route.query.id;
        if (!id) { // 跳转到首页
            this.$router.push({
                path: this.pageMainShow
            });
            return false;
        }
        // 调用微信登陆
        var token = this.wxLoginFunc();
        if (!token) {
            return false;
        }
        // 获取参与信息
        this.assistActorShowFunc(id);
    },
    watch: {
        '$route' (to, from) {
            console.log('路由更新', to, from)
        }
    }
}