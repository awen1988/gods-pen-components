/**
 * 七牛云前端直传
 * 注意：qiniu_config是在页面初始化时请求后端接口获得的，可以根据后端返回修改，必须要有token，参考七牛的文档
 */
return {
  props: {
    // 文件类型
    fileType: {
      default: '*',
      type: String,
      editor: {
        label: '文件类型',
        desc: '可选择上传的文件类型',
        type: 'enum',
        defaultList: {
          '*': '所有文件',
          'image/*': '图片文件',
          'audio/*': '音频文件',
          'video/*': '视频文件'
        }
      }
    },
    // 文件后缀
    fileSuffix: {
      defalut: '',
      type: String,
      editor: {
        label: '文件后缀',
        desc: '可选择上传的文件后缀，不填则不判断',
        type: 'input'
      }
    },
    // 后缀错误提示
    helpFileSuffix: {
      type: String,
      defalut: '请选择指定类型文件',
      editor: {
        label: '错误提示',
        desc: '发生错误时提示信息',
        type: 'input'
      }
    },
    // 文件前缀
    filePrefix: {
      defalut: '',
      type: String,
      editor: {
        label: '文件前缀',
        desc: '上传生成文件前缀，不填则不设置',
        type: 'input'
      }
    },
    // 上传确认
    uploadConfirm: {
      type: Boolean,
      editor: {
        label: '上传确认',
        desc: '选择文件后是否弹出确认提示',
        type: 'checkbox'
      }
    },
    // 确认提示
    helpUploadConfirm: {
      type: String,
      default: '点击确定开始上传',
      editor: {
        work: function () {
          return this.uploadConfirm
        },
        label: '提示信息',
        desc: '确认上传的提示信息',
        type: 'input'
      }
    },
    // 设置字段
    fileField: {
      defalut: '',
      type: String,
      editor: {
        label: '文件字段',
        desc: '上传后通过数据总线设置值的字段',
        type: 'input'
      }
    },
    // 显示组件
    showComponent: {
      defalut: '',
      type: String, // 好像没有直接支持Object
      editor: {
        label: '显示组件',
        desc: '上传完成后需要显示的组件，使用JSON数组格式',
        type: 'data'
      }
    },
    // 隐藏组件
    hideComponent: {
      defalut: '',
      type: Object,
      editor: {
        label: '隐藏组件',
        desc: '上传完成后需要隐藏的组件，使用JSON数组格式',
        type: 'data'
      }
    }
  },
  editorMethods: {
    selectFileFunc: {}
  },
  data: function () {
    return {
      uploadAwait: false
    }
  },
  methods: {
    selectFileFunc: function () {
      var that = this
      console.log('文件类型：', that.fileType)
      // 创建一个虚拟的文件选择元素
      var fileInput = document.createElement('input')
      fileInput.setAttribute('type', 'file')
      fileInput.setAttribute('accept', that.fileType)
      fileInput.click()
      fileInput.addEventListener('change', function (data) {
        // 取得文件
        var file = data.target.files[0]
        console.log('已选文件：', file)
        // 文件后缀
        var fileSuffix = file.name.split('.').splice(-1)
        if (that.fileSuffix) {
          // TODO 判断后缀，暂未完成
        }
        // 文件名字
        console.log('文件前缀：', that.filePrefix)
        var fileName = (new Date()).valueOf() + '.' + fileSuffix;
        if (that.filePrefix) {
          fileName = that.filePrefix + (new Date()).valueOf() + '.' + fileSuffix;
        }
        console.log('文件名：', fileName)
        if (that.uploadConfirm) {
          that.$confirm({title: '系统提示', msg: that.helpUploadConfirm}, function () {
            // console.log('确认')
            that.uploadFileFunc(file, fileName)
          }, function () {
            // console.log('取消')
          });
        } else {
          that.uploadFileFunc(file, fileName)
        }
      })
    },
    uploadFileFunc: function (file, name) {
      var that = this;
      if (that.uploadAwait) {
        return null
      }
      that.uploadAwait = true
      // 显示加载
      that.$loading()
      // 获取七牛云配置，这是后端返回来设置到数据总线的
      var qiniuConfig = that.dataHubGet('qiniu_config');
      if (!qiniuConfig) {
        that.$hideLoading()
        that.$alert({title: '错误提示', msg: '暂无权限，请联系微信：229417598反馈，谢谢~'}, function () {})
        that.uploadAwait = false
        return null
      }
      // 这个是从后端配置拿到的，也可以根据自己情况写死
      var qiniuDomain = qiniuConfig.config.protocol + '://' + qiniuConfig.config.domain + '/'
      // 七牛脚本的位置根据自己情况进行加载
      that.$options.$lib.Util.loadJs('/static/custom/qiniu/qiniu.min.js').then(function () {
        qiniu.upload(file, name, qiniuConfig.token, {}, {
          useCdnDomain: true
        }).subscribe({
          next: function (res) {
            that.$hideLoading()
            that.uploadAwait = false
            console.log('上传情况：', res);
          },
          error: function (error) {
            console.log('报错情况：', error);
            that.$hideLoading()
            that.uploadAwait = false
            that.$alert({title: '错误提示', msg: error.message + '请联系微信：229417598反馈，谢谢~'}, function () {});
          },
          complete: function (res) {
            console.log('完成情况：', res);
            that.$hideLoading()
            that.uploadAwait = false
            that.$alert({title: '系统提示', msg: '上传成功'}, function () {
              // 获得上传后的链接
              var url = qiniuDomain + res.key;
              console.log('文件路径：', url)
              // 设置到数据总线
              if (that.fileField) {
                that.dataHubSet(that.fileField, url)
              }
              // 显示指定组件
              if (that.showComponent) {
                var showComponent = JSON.parse(that.showComponent)
                console.log('显示的组件：', showComponent)
                for (i = 0, length = showComponent.length; i < length; i++) {
                  that.$parent.getComponent(showComponent[i]).$el.style.display = 'block';
                }
              }
              // 隐藏指定组件
              if (that.hideComponent) {
                var hideComponent = JSON.parse(that.hideComponent)
                console.log('隐藏的组件：', showComponent)
                for (i = 0, length = hideComponent.length; i < length; i++) {
                  that.$parent.getComponent(hideComponent[i]).$el.style.display = 'none';
                }
              }
            });
          }
        });
      });
    }
  }
}
