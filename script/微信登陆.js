/**
 * 微信登陆脚本
 * @wxList 微信公众号列表，多个公众号ID用|线分割，实际会处理成数组，用于页面支持多个公众号登陆的情况
 * @wxLoginScope 微信授权模式，交互授权需要自行处理，设置关键词回复，返回链接携带token即可
 * @wxLoginLink 登陆地址，当为授权模式时填写授权地址，交互模式则填写关注引导页，提示用户操作步骤
 * @wxSuccessRedirect 成功回调，授权成功时回调地址，默认为当前页，也可以填写到其他页面，特殊需求
 * @wxErrorRedirect 授权出错的回调页面，不填则接口自行处理
 * 注意：成功回调和失败回调都需要后端接口来处理
 * 返回格式：&wx_id=id&wx_token=token，wx_id不是必须的，如果你只设置了一个公众号，后端返回的时候没必要返回这个，仅为了满足我的需求。
 * 后续请求把token带回去就可以了，token是经过加密的，目前加密有wx_id，所以只携带token即可
 * 幽壑潜蛟 <229417598@qq.com>
 */

return {
    props: {
        // 微信公众号列表
        wxList: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '账号配置',
                desc: '用于登陆的微信公众号ID，多个ID之间用|分隔'
            }
        },
        // 登陆授权模式
        wxLoginScope: {
            type: String,
            default: 'snsapi_userinfo',
            editor: {
                type: 'enum',
                label: '授权模式',
                desc: '注意授权模式需要公众号支持',
                defaultList: {
                    'snsapi_message': '交互授权',
                    'snsapi_base': '静默授权',
                    'snsapi_userinfo': '用户授权'
                }
            }
        },
        // 登陆地址
        wxLoginLink: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '接口地址',
                desc: '授权登陆的接口地址，交互授权模式请跳转到公众号关注引导页'
            }
        },
        // 成功回调
        wxSuccessRedirect: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '成功回调',
                desc: '授权登陆成功时回调地址，不填则使用当前地址'
            }
        },
        // 失败回调
        wxErrorRedirect: {
            type: String,
            default: '',
            editor: {
                type: 'input',
                label: '失败回调',
                desc: '授权登陆失败时回调地址，不填则使用授权接口设置的错误信息'
            }
        },
    },
    methods: {
        wxLoginFunc: function () {
            // 当前链接
            var nowLink = window.location.href;
            var wxId = this.wxIdFunc();
            var wxToken = this.wxTokenFunc();
            if (wxToken) {
                // 进行安全处理
                this.wxSafeFunc();
                return wxToken;
            }
            var redirectLink = this.wxLoginLink;
            if (!redirectLink) {
                // 提示错误
                this.$toast('授权地址错误');
                return false;
            }
            // 跳转到授权页
            if (this.wxLoginScope != 'snsapi_message') {
                redirectLink += redirectLink.indexOf('?') == -1 ? '?' : '&';
                redirectLink += 'wx_id=' + wxId + 'wx_scope=' + this.wxLoginScope;
                if (this.wxSuccessRedirect) { // 成功回调
                    redirectLink += '&success=' + btoa(encodeURI(this.wxSuccessRedirect));
                } else { // 回调到当前页
                    redirectLink += '&success=' + btoa(encodeURI(nowLink));
                }
                if (this.wxErrorRedirect) {
                    redirectLink += '&error=' + btoa(encodeURI(this.wxErrorRedirect));
                }
            }
            // console.log('登陆地址：', redirectLink)
            window.location.href = redirectLink;
            return false;
        },
        // 处理微信账号列表
        wxListFunc: function () {
            var wxList = [];
            if (this.wxList.indexOf('|') !== -1) {
                wxList = this.wxList.split('|');
            } else {
                wxList = [parseInt(this.wxList)];
            }
            return wxList;
        },
        // 获取微信ID
        wxIdFunc: function () {
            var wxId = parseInt(this.$route.query.wx_id) || 0;
            var wxList = this.wxListFunc();
            if (!wxId || !wxList.includes(wxId)) {
                wxId = wxList[0];
            }
            return wxId;
        },
        // 获取微信TOKEN
        wxTokenFunc: function () {
            // 获取URL中的ID
            var wxId = this.wxIdFunc();
            // 获取URL中的TOKEN
            var wxToken = this.$route.query.wx_token || this.dataHubGet('wx_token');
            if (wxId && wxToken) {
                localStorage.setItem('wx_token_' + wxId, wxToken);
                return wxToken;
            }
            wxToken = this.wxLocalTokenFunc(wxId);
            // 设置到数据总线
            this.dataHubSet('wx_token', wxToken);
            return wxToken;
        },
        // 获取微信本地TOKEN
        wxLocalTokenFunc: function (wxId) {
            wxId = wxId || 0;
            var wxToken = '';
            var wxList = this.wxListFunc();
            if (!wxList.length) {
                return wxToken;
            }
            // 传入微信ID则直接获取
            if (wxId) {
                if (wxList.includes(wxId)) {
                    wxToken = localStorage.getItem('wx_token_' + wxId);
                }
                return wxToken;
            }
            // 未传入则循环获取，直至取中
            for (var i = 0, wxListLength = wxList.length; i < wxListLength; i++) {
                wxId = parseInt(wxList[i]);
                if (wxId) {
                    wxToken = localStorage.getItem('wx_token_' + wxId);
                    if (wxToken) {
                        break;
                    }
                }
            }
            return wxToken;
        },
        // 安全处理方法
        wxSafeFunc: function () {
            var nowLink = window.location.href;
            // 进行安全处理
            var safeLink = nowLink.replace(/[?|&]wx_token=[%+-_0-9a-zA-z]*/, '');
            // console.log('安全连接：', safeLink)
            if (nowLink !== safeLink) {
                // 清除历史记录，避免回退
                window.history.forward(1);
                window.location.href = safeLink;
                return false;
            }
            return true;
        },
    }
}
