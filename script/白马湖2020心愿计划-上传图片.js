/**
 * 上传图片
 */
return {
  editorMethods: {
    uploadImageFunc: {}
  },
  methods: {
    uploadImageFunc: function () {
      var that = this;
      // 选择文件
      that.selectFileFunc('image/*')
    },
    selectFileFunc: function (accept = '*') {
      var that = this
      // 创建一个虚拟的文件选择元素
      var fileInput = document.createElement('input')
      fileInput.setAttribute('type', 'file')
      fileInput.setAttribute('accept', accept)
      fileInput.click()
      fileInput.addEventListener('change', function (data) {
        // 取得文件
        var file = data.target.files[0]
        // 文件后缀
        var fileSuffix = file.name.split('.').splice(-1);
        /*
        if (!/(jpg|jpeg)/.test(fileSuffix)) {
          that.$alert({title: '错误提示', msg: '请选择一个图片文件'}, function () {});
          return null;
        }
        */
        // 取得名字
        var name = 'votev2-image-' + (new Date()).valueOf() + '.' + fileSuffix;
        // console.log('文件名：', name)
        that.$confirm({ title: '系统提示', msg: '您已选择文件，是否确认上传'}, function () {
          // console.log('确认')
          that.uploadFileFunc(file, name)
        }, function () {
          // console.log('取消')
        });
      })
    },
    uploadFileFunc: function (file, name) {
      var that = this;
      // 显示加载
      that.$loading()
      // 获取七牛云配置
      var qiniuConfig = that.dataHubGet('qiniu_config');
      if (!qiniuConfig) {
        that.$hideLoading()
        that.$alert({title: '错误提示', msg: '暂无权限，请联系微信：229417598反馈，谢谢~'}, function () {});
        return null
      }
      var qiniuDomain = qiniuConfig.config.protocol + '://' + qiniuConfig.config.domain + '/'
      that.$options.$lib.Util.loadJs('/static/custom/qiniu/qiniu.min.js').then(function () {
        qiniu.upload(file, name, qiniuConfig.token, {}, {
          useCdnDomain: true
        }).subscribe({
          next: function (res) {
            that.$hideLoading()
            // console.log('上传情况：', res);
          },
          error: function (error) {
            // console.log('报错情况：', error);
            that.$hideLoading()
            that.$alert({title: '错误提示', msg: error.message + '请联系微信：229417598反馈，谢谢~'}, function () {});
          },
          complete: function (res) {
            // console.log('完成情况：', res);
            that.$hideLoading()
            that.$alert({title: '系统提示', msg: '上传成功'}, function () {
              // 获得上传后的链接
              var url = qiniuDomain + res.key;
              // console.log('文件路径：', url)
              // 设置到数据总线
              that.dataHubSet('actor_image', url)
              // 获取选择组件
              // var selectComponent = that.$parent.getComponent('truck/image1v').$refs['truck/image1v'];
              // 隐藏当前组件
              // selectComponent.$parent.$el.style.display = 'none';
              that.$parent.getComponent('truck/image1vmrov').$el.style.display = 'none';
              that.$parent.getComponent('truck/image1vmrovg7').$el.style.display = 'block';
            });
          }
        });
      });
    }
  }
}
