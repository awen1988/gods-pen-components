/**
 * 在vue的
 */
return {
  data: function () {
    return {
      // 活动ID
      main_id: 1
    }
  },
  created: function () {
    var that = this
    // 微信登陆
    that.wxLoginFunc()
  },
  methods: {
    /**
     * 判断是否为微信客户端
     * @returns {boolean}
     */
    isWxClientFunc: function () {
      var ua = window.navigator.userAgent.toLowerCase()
      if (ua.match(/MicroMessenger/i) == 'micromessenger') { // eslint-disable-line
        return true
      }
      return false
    },
    /**
     * 微信登陆操作
     */
    wxLoginFunc: function () {
      var that = this
      if (!that.isWxClientFunc()) {
        return false
      }
      var wx_user_token = that.$route.query.wx_user_token
      if (wx_user_token) {
        // 存入本地缓存
        localStorage.setItem('wx_user_token_' + that.main_id, wx_user_token)
        that.wxSafeFunc()
        return true
      }
      wx_user_token = localStorage.getItem('wx_user_token_' + that.main_id)
      if (wx_user_token) {
        return true
      }
      // 去请求user_token
      window.location.href = 'https://weixin.jlqfybjy.xyz/assistv2/main/wx_auth/id/' + that.main_id + '?callback=' + encodeURIComponent(window.location.href)
      return false
    },
    /**
     * 微信安全连接处理
     */
    wxSafeFunc: function () {
      // 抹掉URL数据
      var nowLink = window.location.href;
      var newLink = nowLink.replace(/[?|&]wx_user_token=[%+-_0-9a-zA-z|=]*/, '')
      if (nowLink !== newLink) {
        // region 禁止浏览器后退
        history.pushState(null, null, newLink)
        window.addEventListener('popstate', function () {
          history.pushState(null, null, newLink)
        })
        // endregion
        window.location.href = newLink
      }
      return false
    },
    /**
     * 主数据初始化
     */
    mainInitFunc: function () {

    }
  }
}
